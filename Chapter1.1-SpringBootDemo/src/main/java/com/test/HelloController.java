/*
 * Copyright (c) 2020.
 * Author: huangjuntao
 * Department: the platform department
 *
 */

package com.test;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @RequestMapping("/hello")
    public String hello() {
        return "hello world";
    }

    @RequestMapping("/hello/{name}")
    public String hello(@PathVariable String name) {
        return "hello world , " + name;
    }
}
