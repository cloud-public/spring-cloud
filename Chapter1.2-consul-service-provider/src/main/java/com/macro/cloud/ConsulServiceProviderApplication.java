/*
 * Copyright (c) 2020.
 * Author: Huangjuntao
 * Department: Platform Department
 */

package com.macro.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class ConsulServiceProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsulServiceProviderApplication.class, args);
    }

}
