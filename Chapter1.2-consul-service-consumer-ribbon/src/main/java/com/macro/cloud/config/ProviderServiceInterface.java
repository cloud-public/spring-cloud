/*
 * Copyright (c) 2020.
 * Author: Huangjuntao
 * Department: Platform Department
 */

package com.macro.cloud.config;
import com.macro.cloud.domain.CommonResult;
import com.macro.cloud.domain.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * Spring Cloud服务间通讯方式：声明式服务调用方式
 * 通过Spring Cloud Feign，我们只需创建一个接口并用注解的方式来配置它，
 * 无须像上面两种方式那样需要拼接URL和各种参数，
 * 就能完成对服务提供方的接口绑定，减少服务调用客户端的开发量。
 *
 * */
@FeignClient(name = "consul-user-service")
@RequestMapping("/user")
public interface ProviderServiceInterface {
    @GetMapping("/{id}")
    CommonResult getUser(@PathVariable("id") Long id);
    @GetMapping("/getByUsername")
    CommonResult getByUsername(@RequestParam String username) ;
    @GetMapping("/getEntityByUsername")
    CommonResult getEntityByUsername(@RequestParam String username) ;

    @PostMapping("/create")
    CommonResult create(@RequestBody User user) ;

    @PostMapping("/update")
    CommonResult update(@RequestBody User user) ;
    @PostMapping("/delete/{id}")
    CommonResult delete(@PathVariable Long id) ;
}
