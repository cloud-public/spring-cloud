/*
 * Copyright (c) 2020.
 * Author: Huangjuntao
 * Department: Platform Department
 */

package com.macro.cloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.AsyncRestTemplate;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RibbonConfig {
    /**
     * Spring Cloud服务间通讯方式：同步方式
     * 注入RestTemplate到spirng容器，这样其它类再使用就可以直接用注解的形式使用
     */
    @Bean //注入bean到spring容器
    @LoadBalanced //实现客户端负载均衡
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
    /**
     * Spring Cloud服务间通讯方式：异步方式
     * 注入RestTemplate到spirng容器，这样其它类再使用就可以直接用注解的形式使用
     */
    @Bean //注入bean到spring容器
    @LoadBalanced //实现客户端负载均衡
    public AsyncRestTemplate getAsyncRestTemplate() {
        return new AsyncRestTemplate();
    }
}
