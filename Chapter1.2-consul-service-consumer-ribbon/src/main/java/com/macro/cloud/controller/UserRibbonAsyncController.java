/*
 * Copyright (c) 2020.
 * Author: Huangjuntao
 * Department: Platform Department
 */

package com.macro.cloud.controller;

import com.macro.cloud.domain.AsyncListenableFutureCallback;
import com.macro.cloud.domain.CommonResult;
import com.macro.cloud.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.AsyncRestTemplate;

import java.util.Arrays;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/async/user")
public class UserRibbonAsyncController {
    @Autowired
    private AsyncRestTemplate asyncRestTemplate;
    @Value("${service-url.consul-user-service}")
    private String userServiceUrl;

    @GetMapping("/{id}")
    public CommonResult getUser(@PathVariable Long id) throws ExecutionException, InterruptedException {
        ListenableFuture<ResponseEntity<CommonResult>> future = asyncRestTemplate.getForEntity(userServiceUrl + "/user/{1}", CommonResult.class, id);
        if (future.get().getStatusCode().is2xxSuccessful()) {
            return future.get().getBody();
        } else {
            return new CommonResult("操作失败", 500);
        }
    }

    @GetMapping("/getByUsername")
    public CommonResult getByUsernameL(@RequestParam String username) throws ExecutionException, InterruptedException {
        ListenableFuture<ResponseEntity<CommonResult>> future = asyncRestTemplate.getForEntity(userServiceUrl + "/user/getByUsername?username={1}", CommonResult.class, username);
        if (future.get().getStatusCode().is2xxSuccessful()) {
            return future.get().getBody();
        } else {
            return new CommonResult("操作失败", 500);
        }
    }

    @PostMapping("/create")
    public void create(@RequestBody User user) {
        HttpEntity<User> request = createRequest(user);
        ListenableFuture<ResponseEntity<User>> responseEntityListenableFuture = asyncRestTemplate.postForEntity(userServiceUrl + "/user/create", request, User.class);
        responseEntityListenableFuture.addCallback(new AsyncListenableFutureCallback<>());
    }

    @PostMapping("/update")
    public void update(@RequestBody User user) {
        HttpEntity<User> request = createRequest(user);
        ListenableFuture<ResponseEntity<User>> responseEntityListenableFuture = asyncRestTemplate.postForEntity(userServiceUrl + "/user/update", request, User.class);
        responseEntityListenableFuture.addCallback(new AsyncListenableFutureCallback<>());
    }

    @PostMapping("/delete/{id}")
    public void delete(@PathVariable Long id) {
        ListenableFuture<ResponseEntity<Long>> responseEntityListenableFuture = asyncRestTemplate.postForEntity(userServiceUrl + "/user/delete/" + id, null, Long.class);
        responseEntityListenableFuture.addCallback(new AsyncListenableFutureCallback<>());
    }


    private <T> HttpEntity<T> createRequest(T entity) {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<T> httpEntity = new HttpEntity<>(entity, requestHeaders);
        return httpEntity;
    }
}
