/*
 * Copyright (c) 2020.
 * Author: Huangjuntao
 * Department: Platform Department
 */

package com.macro.cloud.controller;

import com.macro.cloud.config.ProviderServiceInterface;
import com.macro.cloud.domain.CommonResult;
import com.macro.cloud.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/feign/user")
public class UserRibbonFeignController {
    /**
     * Spring Cloud服务间通讯方式：声明式服务调用方式
     * 通过Spring Cloud Feign，我们只需创建一个接口并用注解的方式来配置它，
     * 无须像上面两种方式那样需要拼接URL和各种参数，
     * 就能完成对服务提供方的接口绑定，减少服务调用客户端的开发量。
     *
     * */
    @Autowired
    private ProviderServiceInterface providerServiceInterface;
    @RequestMapping("/{id}")
    public CommonResult getUser(@PathVariable Long id) {
        return providerServiceInterface.getUser(id);
    }

    @GetMapping("/getByUsername")
    public CommonResult getByUsername(@RequestParam String username) {
        return providerServiceInterface.getByUsername(username);
    }

    @PostMapping("/create")
    public CommonResult create(@RequestBody User user) {
        return providerServiceInterface.create(user);
    }

    @PostMapping("/update")
    public CommonResult update(@RequestBody User user) {
        return providerServiceInterface.update(user);
    }

    @PostMapping("/delete/{id}")
    public CommonResult delete(@PathVariable Long id) {
        return providerServiceInterface.delete(id);
    }


}
