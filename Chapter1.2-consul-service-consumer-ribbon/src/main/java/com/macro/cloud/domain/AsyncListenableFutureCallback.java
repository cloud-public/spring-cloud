/*
 * Copyright (c) 2020.
 * Author: Huangjuntao
 * Department: Platform Department
 */

package com.macro.cloud.domain;

import org.springframework.util.concurrent.ListenableFutureCallback;

import org.apache.log4j.Logger;

public class AsyncListenableFutureCallback<T> implements ListenableFutureCallback<T> {
    static Logger log = Logger.getLogger(AsyncListenableFutureCallback.class);


    @Override
    public void onFailure(Throwable throwable) {
        log.error("failed to send data", throwable);

    }

    @Override
    public void onSuccess(T t) {
        log.debug("Success sending data: "+t);

    }
}
